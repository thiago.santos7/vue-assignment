# vue-assignment

This project is about a countdown app. It lets you start, stop and save the current timer. A fake json-server API is included in json-server-api folder.

## Tools

- Vue 3;
- Vue CLI;
- Vue Test Utils;
- Vue Jest;
- FortAwesome.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
