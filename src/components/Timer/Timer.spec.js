import { mount } from "@vue/test-utils";
import { nextTick } from "vue";
import Timer from "./Timer.vue";

beforeEach(() => {
  jest.useFakeTimers();
});

describe("Timer", () => {
  it("should render Timer with correct values", () => {
    const wrapper = mount(Timer, { shallow: true });

    expect(wrapper.text()).toContain("0 seconds");
    expect(wrapper.findAll("button").length).toBe(2);
  });

  it("should emit event on save button click", async () => {
    const wrapper = mount(Timer, { shallow: true });

    await wrapper.findAll("button")[1].trigger("click");

    expect(wrapper.emitted()).toHaveProperty("save-timer");
  });

  it("should start counter on start click", async () => {
    const wrapper = mount(Timer, { shallow: true });

    await wrapper.findAll("button")[0].trigger("click");

    jest.advanceTimersByTime(10000);

    await nextTick();

    expect(wrapper.text().replace("Stop", "")).not.toBe("0 seconds");
  });
});
